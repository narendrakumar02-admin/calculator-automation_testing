package com.test.july21Batch.calculator.controller.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerExceptionResolver;

@Configuration
public class SentryConfig {
	
	@Bean
	public HandlerExceptionResolver sentryExceptionResolver() {
	  return new io.sentry.spring.SentryExceptionResolver();
	}

}
 